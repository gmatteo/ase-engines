from pathlib import Path


def main():
    from ase.build import bulk
    directory = Path('work/nwchem')
    directory.mkdir(exist_ok=True, parents=True)

    atoms = bulk('Si')
    path = atoms.cell.bandpath('GXWK', density=15)
    path.write('path.json')

    from ase.calculators.nwchem import NWChem


    calc = NWChem(kpts=[4, 4, 4], bandpath=path, directory=directory)
    atoms.calc = calc
    atoms.get_potential_energy()

    # We are missing the Fermi level.  May need to set smearing.
    bs = calc.band_structure()
    bs.write(directory / 'bs.json')


if __name__ == '__main__':
    main()
