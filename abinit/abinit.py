from ase.calculators.genericfileio import (
    GenericFileIOCalculator, CalculatorTemplate, read_stdout)
import os
from abipy.abilab import AbinitInput, Structure
from abipy.abio.factories import BandsFromGsFactory
import numpy as np
from abipy.abilab import abiopen
# Environment variables:
# ABINIT_PP_PATH
# ASE_ABINIT_COMMAND

class AbipyProfile:

    def __init__(self, argv):
        self.argv = tuple(argv)

    def run(self, directory, inputfile, outputfile):
        from subprocess import check_call
        import os
        argv = list(self.argv) + [str(inputfile)]
        with open(directory / outputfile, 'wb') as fd:
            check_call(argv, cwd=directory, stdout=fd, env=os.environ)

class AbipyTemplate(CalculatorTemplate):

    def __init__(self):
        super().__init__(
            'abinit',
            ['energy', 'free_energy', 'forces', 'stress', 'magmoms'])

        self.name = 'calc'
        self.inputname = f'{self.name}.abi'
        self.outputname = f'{self.name}.abo'

    def write_input(self, abinit_input, directory):

        # Sanitize: Because there are some parts of the input that are apparently not correct?
        illegal_phrases = ['indata_prefix', 'tmpdata_prefix', 'outdata_prefix']
        sanitized_input = ''
        for line in str(abinit_input).split('\n'):
            legal_line = True
            for illegal_phrase in illegal_phrases:
                if illegal_phrase in line:
                    legal_line = False
            if legal_line:
                sanitized_input += line + '\n'

        with open(directory / self.inputname, 'w') as f:
            print(sanitized_input, file=f)

        # Save abipy input as an attribute so it can retrieved.
        # Could lead to unexpected results if the same Template object is used for several calculations.
        # Not sure if this function is allowed to return it though.
        self.abipy_input = abinit_input

    def execute(self, directory, profile):
        profile.run(directory, self.inputname, self.outputname)

    def read_results(self, directory):
        """
        """
        properties = dict()

        for read_func in [self.read_groundstate, self.read_gw]:
            properties.update(read_func(directory))

        return properties

    def read_groundstate(self, directory):
        """
        Read results from groundstate calculation.

        Will read:
            energy, eigenvalues, ibz_kpoints and fermi level.

        Parameters:
        -----------
        directory: str
            Directory containing abinit GSR file.

        Returns:
        --------
        dict
        """
        file_path = directory / f'{self.name}o_GSR.nc'
        properties = {}
        if not os.path.exists(file_path):
            return properties

        with abiopen(file_path) as gsr:
            properties['nband'] = gsr.nband
            properties['energy'] = gsr.energy

            properties['eigenvalues'] = gsr.ebands.eigens
            properties['ibz_kpoints'] = gsr.ebands.kpoints.to_array()
            #properties['fermi_level'] = gsr.ebands.fermie.tolist()
            properties['fermi_level'] = float(gsr.ebands.fermie)
            properties['atoms'] = gsr.structure.to_ase_atoms()

        return properties

    def read_gw(self, directory):
        """
        Read results from gw calculation.

        Parameters:
        -----------
        directory: str
            Directory where 'SIGRES' file is located.
        """
        gw_file = directory / f'{self.name}o_SIGRES.nc'
        properties = {}
        if not os.path.exists(gw_file):
            return properties

        #with abiopen(gw_file) as gw:
        # for _ in dir(gw):
        #     print(_)

        return properties

class AbipyCalculation:

    def __init__(self, atoms, parameters, directory, abipy_input):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory.resolve()
        self.abipy_input = abipy_input

    def read_results(self):
        template = AbipyTemplate()
        return template.read_results(self.directory)

    def get_file_path(self, file_name):
        return self.directory / file_name

def groundstate(atoms, parameters, pseudo_potentials, directory, execute=True):
    """
    Parameters
    ----------
    atoms: ase.Atoms
        The atoms object to do the calculation for.
    parameters: dict
        Dictionary of parameters to use for the calculation. Any valid
        abinit keyword is allowed.
    pseudo_potentials: list
        List of pseudopotentials to use.
    directory: str
        Directory where the calculation is to be performed.
    execute: bool
        If True, execute the calculation.

    Returns
    -------
    AbipyCalculation
    """

    print('Doing groundstate calculation')

    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()
    gs_directory = directory

    input_dict = dict() # No additional default parameters.
    input_dict.update(parameters)

    # Increase default value of nstep (30)
    if "nstep" not in input_dict:
        input_dict["nstep"] = 50

    abinit_input = AbinitInput(Structure.from_ase_atoms(atoms),
                            pseudos=pseudo_potentials,
                            abi_kwargs=input_dict)

    gs_directory.mkdir(exist_ok=True, parents=True)
    if execute:
        template.write_input(abinit_input, gs_directory)
        template.execute(gs_directory, profile)
    return AbipyCalculation(atoms, parameters=parameters, directory=gs_directory,
                            abipy_input=abinit_input)

def bandstructure(gs, bandpath, parameters, directory, execute=True):
    """
    Parameters
    ----------
    gs: AbipyCalculation
        The groundstate calculation to use as a starting point for the bandstructure calculation.
    bandpath: ase.dft.kpoints.BandPath
        The bandpath to calculate the bandstructure along.
    parameters: dict
        Parameters for the bandstructure calculation.
        Has to contain the key 'abi_kwargs' which is a dictionary of abinit input variables.
        Any other valid abinit input variable can be included, but can drastically!
    directory: str
        Directory where the calculation is to be performed.
    execute: bool
        If True, execute the calculation.
        Mainly included for testing purposes.

    Returns
    -------
    AbipyCalculation
    """
    print('Doing bandstructure calculation')

    factory_kwargs = {}
    den_path = gs.directory / 'calco_DEN' # This assumes the template name is 'calco'

    # MG: Make sure that tolwfr in specified in input.
    band_input = BandsFromGsFactory().build_input(gs.abipy_input, **factory_kwargs)

    # Specific inputs:
    input_dict = dict(
                    getden_filepath=den_path,
                    kptopt=0,
                    kpt=bandpath.kpts,
                    nkpt=len(bandpath.kpts),
                    kptbounds=None,
                    ndivsm=None,
                    )

    input_dict.update(parameters)
    band_input.set_vars(**input_dict)

    directory.mkdir(exist_ok=True, parents=True)
    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()

    if execute:
        template.write_input(band_input, directory)
        template.execute(directory, profile)
    return AbipyCalculation(atoms, parameters, directory,
                            abipy_input=band_input)

def bandgap(gs, parameters, directory, execute=True):
    """
    Parameters
    ----------
    gs: AbipyCalculation
        The groundstate calculation to use as a starting point for the bandgap calculation.
    parameters: dict
        Parameters for the bandgap calculation.
        Has to contain the key 'abi_kwargs' which is a dictionary of abinit input variables.
        Any other valid abinit input variable can be included, but can drastically
        alter what abinit does!
    directory: str
        Directory where the calculation is to be performed.
    execute: bool
        If True, execute the calculation.
        Mainly included for testing purposes.

    Returns
    -------
    AbipyCalculation
    """
    print('Doing bandgap calculation')
    den_path = gs.directory / 'calco_DEN' # Assume template name is 'calco'

    bg_input = gs.abipy_input.deepcopy()
    nband = gs.read_results()['nband']
    input_dict = dict(nband=nband+10,
                    getden_filepath=den_path,
                    iscf=-2)
    input_dict.update(parameters)
    bg_input.set_vars(**input_dict)

    # Make directory, profile and template:
    directory.mkdir(exist_ok=True, parents=True)
    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()

    # Write and execute:
    if execute:
        template.write_input(bg_input, directory)
        template.execute(directory, profile)

    return AbipyCalculation(atoms, parameters, directory,
                            abipy_input=bg_input)

def relaxation(atoms, parameters, pseudo_potentials, directory, execute=True):
    """
    Parameters
    ----------
    atoms: ase.Atoms
        The atoms object to do the calculation for.
    parameters: dict
        Parameters for the relaxation calculation. Can contain any valid abinit
        input variable.
    pseudo_potentials: list
        List of full paths to the pseudos potentials to use.
    directory: str
        Directory where the calculation is to be performed.
    execute: bool
        If True, execute the calculation.
        Mainly included for testing purposes.
    """
    print('Doing relaxation calculation')

    # Input dictionary:
    input_dict = dict(
            ionmov=2,
            optcell=0,   # MG FIXME: This assumes fixed cell, in this case dilatmx is useless.
            ntime=100,
            dilatmx=1.05,
            ecutsm=0.5,
            chksymtnons=0,
            )

    input_dict.update(parameters)
    abinit_input = AbinitInput(Structure.from_ase_atoms(atoms), pseudos=pseudo_potentials,
                            abi_kwargs=input_dict)

    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()
    directory.mkdir(exist_ok=True, parents=True)
    if execute:
        template.write_input(abinit_input, directory)
        template.execute(directory, profile)
    return AbipyCalculation(atoms, parameters=parameters, directory=directory,
                            abipy_input=abinit_input)

def screening_ppmodel(bg, atoms, parameters, directory, execute=True):
    """
    Parameters
    ----------
    bg: AbipyCalculation
        The bandgap calculation to use as a starting point for the screening calculation.
    atoms: ase.Atoms
        The atoms object to do the calculation for.
    parameters: dict
        Parameters for the screening calculation. Any valid abinit input variable can be included,
        but can drastically alter what abinit does!
    Directory:
        Directory where the calculation is to be performed.
    execute: bool
        If True, execute the calculation.
        Mainly included for testing purposes.
    """
    print('Doing screening calculation')
    directory.mkdir(exist_ok=True, parents=True)

    abinit_input = bg.abipy_input.deepcopy()
    wfk_path = bg.directory / 'calco_WFK' # This assumes the template name is 'calc'
    input_dict = dict(
                optdriver=3,
                ecuteps=2,   # MG: FIXME. This should be an input parameter and propagated to the SIGMA part.
                             #            Also, nband is missing and cannot be greater that nband stored in the WFK
                getden_filepath=None,
                getwfk_filepath=wfk_path,
                )

    input_dict.update(parameters)
    abinit_input.set_vars(**input_dict)
    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()
    if execute:
        template.write_input(abinit_input, directory)
        template.execute(directory, profile)
    return AbipyCalculation(atoms, parameters, directory, abinit_input)

def gw(bg, scr, parameters, directory, execute=True):
    """
    Parameters
    ----------
    bg: AbipyCalculation
        The bandgap calculation to use as a starting point for the GW calculation.
    scr: AbipyCalculation
        The screening calculation to use as a starting point for the GW calculation.
    parameters: dict
        Parameters for the GW calculation. Any valid abinit input variable can be included,
        but can drastically alter what abinit does!
    directory: str
        Directory where the calculation is to be performed.
    """

    print('Doing GW calculation')
    # Path stuff:
    scr_path = scr.directory / 'calco_SCR'
    wfk_path = bg.directory / 'calco_WFK'

    gw_input = scr.abipy_input.deepcopy()
    #MG FIXME: Make sure that nband, ecuteps are in parameters.
    input_dict = dict(
                optdriver=4,
                ecutsigx=scr.abipy_input.get('ecut'),
                getscr_filepath=scr_path,
                getwfk_filepath=wfk_path)
    input_dict.update(parameters)
    gw_input.set_vars(input_dict)

    directory.mkdir(exist_ok=True, parents=True)
    profile = AbipyProfile(['abinit'])
    template = AbipyTemplate()
    if execute:
        template.write_input(gw_input, directory)
        template.execute(directory, profile)

    return AbipyCalculation(bg.atoms, parameters, directory, abipy_input=gw_input)

if __name__ == '__main__':

    from ase.build import bulk
    from pathlib import Path

    directory = Path('out_5/')

    atoms = bulk('Si')

    pseudo_potentials = ['/Users/au616397/Desktop/pseudos/Si14.psp8']

    parameters = {
            'ecut': 12,
            'tolwfr': 1e-12,
            'ngkpt': '4 4 4',
            'chksymbreak': 0
            }


    gs_execute = False
    bs_execute = False
    bg_execute = True
    scr_execute = False
    gw_execute = False

    # Ground structure calculation:
    gs_directory = directory / 'gs'
    gs = groundstate(atoms, parameters, pseudo_potentials, gs_directory, execute=gs_execute)

    # Band structure calculation:
    bandpath = atoms.cell.bandpath('GXWK', density=15)
    bandstructure_parameters = {}
    bs_directory = directory / 'bs'
    bs = bandstructure(gs, bandpath, bandstructure_parameters, bs_directory, execute=bs_execute)
    properties = bs.read_results()

    # # Band structure plot and kpoints test:
    # from ase.spectrum.band_structure import BandStructure
    # abinit_kpts = properties['ibz_kpoints']
    # energies = properties['eigenvalues']
    # reference = properties['fermi_level']
    # bandstructure = BandStructure(bandpath, energies-reference, reference=0)
    # kpts_err = abs(bandpath.kpts - abinit_kpts).max()
    # assert kpts_err < 1e-7
    # bandstructure.plot()
    # import matplotlib.pyplot as plt
    # plt.show()

    # Bandgap calculation:
    # bandgap_parameters = dict(abi_kwargs=dict(nband=30))
    bandgap_parameters = dict()
    bg_directory = directory / 'bg'
    bg = bandgap(gs, bandgap_parameters, bg_directory, execute=bg_execute)

    # # Screening calculation:
    # scr_parameters = {}
    # scr_directory = directory / 'scr'
    # scr = screening_ppmodel(bg, atoms, scr_parameters, scr_directory, execute=scr_execute)

    # # # GW calculation:
    # gw_parameters = dict(
    #         nkptgw=0,
    #         #kptgw=np.array([1.25000000E-01, 1.25000000E-01, 1.25000000E-01]),
    #         gw_qprange=5,
    #         )
    # gw_directory = directory / 'gw'
    # gwo = gw(bg, scr, gw_parameters, gw_directory, execute=gw_execute)
    # gwo.read_results()

    # # Relaxation calculation:
    # atoms.positions[0] += 0.25
    # relax_parameters = dict(ecut=8, tolwfr=1e-5)
    # relax_directory = directory / 'relax'
    # rc = relaxation(atoms, relax_parameters, pseudo_potentials, relax_directory)
    # properties = rc.read_results()

    # Plot relaxation initial and final state:
    #from ase.visualize import view
    #view([atoms, properties['atoms']])



